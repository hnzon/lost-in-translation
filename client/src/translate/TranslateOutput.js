import styles from "./Translate.module.css";
import Translation from "./Translation";

function TranslateOutput({ inputString }) {
  return (
    <div className={styles.TranslationBox}>
      <h2>Translation</h2>
      <Translation inputString={inputString} />
    </div>
  );
}

export default TranslateOutput;
