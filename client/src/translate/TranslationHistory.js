import styles from "./Translate.module.css";

import Translations from "./Translations";

function TranslationHistory({ translations }) {
  return (
    <div className={styles.TranslationHistoryBox}>
      <div>
        {translations.length ? (
          <h2>Your last translations</h2>
        ) : (
          <h3>Your translation history is empty</h3>
        )}
      </div>
      <div className={styles.TranslationList}>
        <Translations translations={translations} />
      </div>
    </div>
  );
}

export default TranslationHistory;
