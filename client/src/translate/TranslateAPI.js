import axios from "axios";

const BASE_URL = "http://localhost:8000/users";

const getUser = async (currentUser) => {
  try {
    const response = await axios.get(BASE_URL);
    const users = response.data;
    const user = users.find((user) => user.username === currentUser);
    return user;
  } catch (error) {
    return console.log(error);
  }
};

export const addTranslationToDb = async (userInput, currentUser) => {
  try {
    const user = await getUser(currentUser);

    let translations = [];
    if (user.translations) {
      translations = [
        ...user.translations,
        { text: userInput, deleted: false },
      ];
    } else {
      translations.push({ text: userInput, deleted: false });
    }
    await axios
      .put(`${BASE_URL}/${user.id}`, {
        username: currentUser,
        translations: translations,
      })
      .then(() => {
        console.log(`added "${userInput}" to history`);
      });
  } catch (error) {
    return console.log(error);
  }
};

export const getTranslations = async (currentUser) => {
  try {
    const user = await getUser(currentUser);

    let translations = [];
    let translationStrings = [];
    if (user.translations) {
      translations = user.translations.filter(
        (translation) => translation.deleted === false
      );
      translationStrings = translations.map(({ text }) => [text]);
    } else {
      return translations;
    }

    return translationStrings
      .slice(Math.max(translations.length - 10, 0))
      .reverse();
  } catch (error) {
    return console.log(error);
  }
};

export const setTranslationsAsDeleted = async (currentUser) => {
  try {
    const user = await getUser(currentUser);

    let deletedTranslations = [];
    if (user.translations) {
      deletedTranslations = user.translations.map((item) => {
        const deletedTranslation = {};
        deletedTranslation.text = item.text;
        deletedTranslation.deleted = true;
        return deletedTranslation;
      });
      console.log("deleted translations");
    } else {
      return;
    }
    await axios.patch(`${BASE_URL}/${user.id}`, {
      translations: deletedTranslations,
    });
  } catch (error) {
    return console.log(error);
  }
};
