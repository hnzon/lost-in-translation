import styles from "./Translate.module.css";
import Translation from "./Translation";

function Translations({ translations }) {
  return translations.map((inputString, index) => (
    <div className={styles.TranslationListItem} key={index}>
      {console.log(
        "what is typeof inputString in translations " + typeof inputString
      )}
      <Translation inputString={inputString.toString()} />
    </div>
  ));
}

export default Translations;
