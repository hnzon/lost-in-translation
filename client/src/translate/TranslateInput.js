import styles from "./Translate.module.css";

function TranslateInput(props) {
  const handleInputChange = (e) => {
    props.onUserInputChange(e);
    e.preventDefault();
  };

  const handleSubmit = (e) => {
    props.onSubmit(e);
    e.preventDefault();
  };

  return (
    <section className={styles.InputContainer}>
      <form onSubmit={handleSubmit} className={styles.TranslateForm}>
        <input
          className={styles.TranslateInput}
          type='text'
          autoFocus
          value={props.userInput}
          onChange={handleInputChange}
        />
        <input
          className={styles.TranslateButton}
          type='submit'
          value='Translate'
        />
      </form>
    </section>
  );
}

export default TranslateInput;
