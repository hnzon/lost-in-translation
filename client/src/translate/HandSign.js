function HandSign({ letter }) {
  if (letter === " ") {
    return <div style={{ width: "50px" }}></div>;
  } else if (!letter.match(/[a-z]/)) {
    return <h1>{letter}</h1>;
  } else {
    return (
      <img
        src={require(`../assets/signs/${letter}.png`).default}
        style={{
          height: "50px",
          width: "50px",
          margin: "1px",
          display: "block",
        }}
      />
    );
  }
}

export default HandSign;
