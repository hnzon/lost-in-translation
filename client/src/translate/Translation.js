import HandSign from "./HandSign";
import styles from "./Translate.module.css";

function Translation({ inputString }) {
  const inputChars = inputString.toLowerCase().split("");

  const translatedSignTxt = inputChars.map(function (letter, index) {
    return <HandSign letter={letter} key={index} />;
  });
  return (
    <div className={styles.Translation}>
      <div>
        <p>{inputString}</p>
      </div>
      <div className={styles.TranslatedText}>{translatedSignTxt}</div>
    </div>
  );
}

export default Translation;
