import { useState } from "react";
import { useAuth } from "../context/AuthContext";
import styles from "./Translate.module.css";
import { addTranslationToDb } from "./TranslateAPI";
import TranslateInput from "./TranslateInput";
import TranslateOutput from "./TranslateOutput";

function Translate() {
  const { authToken } = useAuth();
  const currentUser = window.atob(authToken);
  const [userInput, setUserInput] = useState("");
  const [submittedString, setSubmittedString] = useState("");

  const handleUserInputChange = (e) => {
    setUserInput(e.target.value);
  };

  const handleSubmit = async (e) => {
    if (userInput === "") {
      console.log("nothing entered");
    } else {
      setSubmittedString(userInput);
      await addTranslationToDb(userInput, currentUser);
      setUserInput("");
      e.preventDefault();
    }
  };

  return (
    <>
      <div className={styles.InputContainer}>
        <TranslateInput
          onUserInputChange={handleUserInputChange}
          onSubmit={handleSubmit}
        />
      </div>
      <div className={styles.OutputContainer}>
        <TranslateOutput inputString={submittedString} />
      </div>
    </>
  );
}

export default Translate;
