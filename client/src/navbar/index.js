import styles from "./Navbar.module.css";
import React from "react";
import { Link } from "react-router-dom";
import { useAuth } from "../context/AuthContext";

function Navbar() {
  const { authToken } = useAuth();
  return (
    <nav className='navbar'>
      <Link to='/translate'>
        <div className='Header'>
          <img
            className={styles.HeaderImg}
            src={require("../assets/img/Logo.png").default}
          ></img>

          <h1 className='HeaderTxt'>Lost in Translation</h1>
        </div>
      </Link>

      {authToken && (
        <Link to='/profile'>
          <h2>{window.atob(authToken)}</h2>
        </Link>
      )}
    </nav>
  );
}

export default Navbar;
