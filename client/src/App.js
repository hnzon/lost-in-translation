import "./App.css";
import { useState } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { AuthContext } from "./context/AuthContext";
import Navbar from "./navbar";
import Login from "./login/Login";
import Translate from "./translate";
import Profile from "./profile";
import PrivateRoute from "./PrivateRoute";

function App() {
  const [authToken, setAuthToken] = useState(
    localStorage.getItem("token") || ""
  );

  const setToken = (data) => {
    localStorage.setItem("token", data);
    setAuthToken(data);
  };

  return (
    <AuthContext.Provider value={{ authToken, setAuthToken: setToken }}>
      <BrowserRouter>
        <div className='App'>
          <Navbar />
          <Switch>
            <Route exact path='/' component={Login} />
            <PrivateRoute path='/translate' component={Translate} />
            <PrivateRoute path='/profile' component={Profile} />
          </Switch>
        </div>
      </BrowserRouter>
    </AuthContext.Provider>
  );
}

export default App;
