import React, { useState } from "react";
import { useAuth } from "../context/AuthContext";
import { Redirect } from "react-router-dom";
import { checkUsername, createUser } from "./LoginAPI";

function Login() {
  const [username, setUsername] = useState("");
  const { authToken, setAuthToken } = useAuth();

  const onLoginClick = async () => {
    if (username === "") return;
    try {
      const foundUser = await checkUsername(username);
      if (foundUser) {
        console.log(`found user ${username}`);
        setAuthToken(window.btoa(username));
        return;
      }

      const createdUser = await createUser(username);
      if (createdUser) {
        console.log(`created user ${username}`);
        setAuthToken(window.btoa(username));
      }
    } catch (e) {
      console.log(e);
    }
  };

  if (authToken) {
    return <Redirect to='translate' />;
  }

  return (
    <form>
      <h3>Sign in</h3>
      <input
        type='text'
        placeholder='Username'
        onChange={(e) => setUsername(e.target.value)}
      />
      <button type='button' onClick={onLoginClick}>
        Login
      </button>
    </form>
  );
}

export default Login;
