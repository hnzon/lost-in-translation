import styles from "../translate/Translate.module.css";
import React, { useEffect, useState } from "react";
import { useAuth } from "../context/AuthContext";
import TranslationHistory from "../translate/TranslationHistory";
import {
  getTranslations,
  setTranslationsAsDeleted,
} from "../translate/TranslateAPI";

function Profile() {
  const { authToken, setAuthToken } = useAuth();
  const currentUser = window.atob(authToken);
  const [translations, setTranslations] = useState([]);

  useEffect(() => {
    async function getTranslationsFromAPI() {
      const translationsFromDb = await getTranslations(currentUser);
      setTranslations(translationsFromDb || []);
    }
    getTranslationsFromAPI();
  }, [currentUser]);

  function logOut() {
    console.log(authToken);
    localStorage.clear();
    setAuthToken();
  }

  function clearTranslations() {
    setTranslationsAsDeleted(currentUser);
    setTranslations([]);
  }

  return (
    <>
      <h1>{currentUser}</h1>

      <div className={styles.OutputContainer}>
        <TranslationHistory
          currentUser={currentUser}
          translations={translations}
        />
      </div>
      {translations.length > 0 && (
        <button className={styles.ClearButton} onClick={clearTranslations}>
          Clear Translations
        </button>
      )}
      {console.log(translations)}
      <button className={styles.LogoutButton} onClick={logOut}>
        Log out
      </button>
    </>
  );
}

export default Profile;
